module Schema
    def self.create
        ActiveRecord::Schema.define do 
            create_table :movies, force: true do |table|
                table.string :name, :lastname, :address
                table.integer :date 
            end
        end
    end
end