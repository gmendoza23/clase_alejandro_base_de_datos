require 'active_record'

module Connection
    def self.connect
        ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'movies_db')
    end
end